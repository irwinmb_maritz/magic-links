import React from 'react';
import { withRouter } from 'react-router-dom';
import './LoginForm.css';
import { API_BASE_URL } from '../../constants/apiConstants';
import axios from 'axios'


const SEND_MAGIC_LINK_SERVICE_URL = API_BASE_URL + '/magic-link';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            emailAddress: "",
            password: "",
            successMessage: "",
            user: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.redirectToRegister = this.redirectToRegister.bind(this);
        this.sendMagicLinkWithAxios = this.sendMagicLinkWithAxios.bind(this);
    }

    handleChange(event) {
        const {id , value} = event.target   
        this.setState(prevState => ({
            ...prevState,
            [id] : value
        }))
        console.log(JSON.stringify(this.state));
    }

    sendMagicLinkWithAxios() {
        axios.post(SEND_MAGIC_LINK_SERVICE_URL + "/" + this.state.emailAddress)
        .then(response => {
            this.setState({successMessage: 'Link has been sent...'})
        })
        .catch(e => {
            console.log(e);
        })
    };

    handleSubmit(event) {
        event.preventDefault();
        this.sendMagicLinkWithAxios();
    }

    redirectToRegister() {
        this.props.history.push('/register'); 
    }

    render () {
        return (
            <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
                <form>
                    <div className="form-group text-left">
                    <label htmlFor="exampleInputEmail1">Email address</label>
                    <input type="email" 
                        className="form-control" 
                        id="emailAddress" 
                        aria-describedby="emailHelp" 
                        placeholder="Enter email" 
                        value={this.state.emailAddress}
                        onChange={this.handleChange}
                    />
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div className="form-group text-left">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" 
                        className="form-control" 
                        id="password" 
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleChange} 
                    />
                    </div>
                    <div className="form-check">
                    </div>
                    <button 
                        type="submit" 
                        className="btn btn-primary"
                        onClick={this.handleSubmit}
                    >Submit</button>
                </form>
                <div className="alert alert-success mt-2" style={{display: this.state.successMessage ? 'block' : 'none' }} role="alert">
                    {this.state.successMessage}
                </div>
                <div className="registerMessage">
                    <span>Dont have an account? </span>
                    <span className="loginText" onClick={this.redirectToRegister}>Register</span> 
                </div>
            </div>
        )
    }

}

export default withRouter(LoginForm);