import React from 'react';
import { withRouter } from 'react-router-dom';
import { ACCESS_TOKEN_NAME, API_BASE_URL } from '../../constants/apiConstants';
import axios from 'axios';
import queryString from 'query-string';


const USER_SERVICE_URL = API_BASE_URL + '/users/findByEmail';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            data: []
        };
    }

    componentDidMount() {
        this.fetchUser();
    }

    fetchUserWithAxios = () => {
        this.setState({...this.state, isFetching: true});
        let params = queryString.parse(this.props.location.search);

        axios.get(USER_SERVICE_URL + "/" + params.emailAddress, {headers: { 'token': localStorage.getItem(ACCESS_TOKEN_NAME) }})
        .then(response => {
            this.setState({data: response.data, isFetching: false})
        })
        .catch(e => {
            console.log(e);
            this.setState({...this.state, isFetching: false});
        })
    };

    fetchUser = this.fetchUserWithAxios;

    render () {
        return (
            <div>Hello, {this.state.data.firstName}</div>
        )
    }
}

export default withRouter(Home);