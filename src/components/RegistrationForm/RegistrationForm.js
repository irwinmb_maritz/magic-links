import axios from 'axios';
import React, { useState } from 'react';
import { API_BASE_URL } from '../../constants/apiConstants';
import { withRouter } from 'react-router-dom';

function RegistrationForm(props) {
    const [state, setState] = useState({
        username: "",
        firstName: "",
        email: "",
        password: "",
        confirmPassword: "",
        phone: "",
        deviceId: "",
        successMessage: null
    })

    const handleChange = (e) => {
        const {id, value} = e.target
        setState(prevState => ({
            ...prevState,
            [id]: value
        }))
    }

    const sendDetailsToServer = () => {
        if (state.email.length && state.password.length) {
            props.showError(null);
            const payload = {
                "username": state.username,
                "firstName": state.firstName,
                "email": state.email,
                "password": state.password,
                "phone": state.phone,
                "deviceId": state.deviceId,
            }
            axios.post(API_BASE_URL + '/users', payload)
            .then(function (response) {
                console.log("Response Status: " + response.status)
                if (response.status === 200) {
                    setState(prevState => ({
                        ...prevState,
                        'successMessage': 'Registration successful. Redirecting to home page...'
                    }))
                    redirectToLogin();
                    props.showError(null)
                } else {
                    props.showError("Some error occurred");
                }
            })
            .catch(function (error) {
                console.log(error);
            });
        } else {
            props.showError('Please enter valid username and password')
        }
    }

    const redirectToLogin = () => {
        props.updateTitle('Login');
        props.history.push('/login');
    }

    const handleSubmitClick = (e) => {
        e.preventDefault();
        if(state.password === state.confirmPassword) {
            sendDetailsToServer()
        } else {
            console.log("Password do not match");
            props.showError('Passwords do not match');
        }
    }

    return (
        <div className="card col-12 col-lg-4 login-card mt-2 hv-center">
            <form>
                <div className="form-group text-left">
                    <label htmlFor="usernameInput">Username</label>
                    <input type="text"
                           className="form-control"
                           id="username"
                           placeholder="Enter username"
                           value={state.username}
                           onChange={handleChange}
                    />
                </div>
                <div className="form-group text-left">
                    <label htmlFor="firstNameInput">First Name</label>
                    <input type="text"
                           className="form-control"
                           id="firstName"
                           placeholder="Enter First Name"
                           value={state.firstName}
                           onChange={handleChange}
                    />
                </div>
                <div className="form-group text-left">
                    <label htmlFor="emailInput">Email Address</label>
                    <input type="email"
                           className="form-control"
                           id="email"
                           aria-describedby="emailHelp"
                           placeholder="Enter email"
                           value={state.email}
                           onChange={handleChange}
                    />
                    <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div className="form-group text-left">
                    <label htmlFor="passwordInput">Password</label>
                    <input type="password"
                           className="form-control"
                           id="password"
                           placeholder="Password"
                           value={state.password}
                           onChange={handleChange}
                    />
                </div>
                <div className="form-group text-left">
                    <label htmlFor="confirmPasswordInput">Confirm Password</label>
                    <input type="password"
                           className="form-control"
                           id="confirmPassword"
                           placeholder="Confirm Password"
                           value={state.confirmPassword}
                           onChange={handleChange}
                    />
                </div>
                <div className="form-group text-left">
                    <label htmlFor="phoneInput">Phone</label>
                    <input type="text"
                           className="form-control"
                           id="phone"
                           placeholder="Enter Phone Number"
                           value={state.phone}
                           onChange={handleChange}
                    />
                </div>
                <div className="form-group text-left">
                    <label htmlFor="deviceIdInput">Device ID</label>
                    <input type="text"
                           className="form-control"
                           id="deviceId"
                           placeholder="Enter Device ID"
                           value={state.deviceId}
                           onChange={handleChange}
                    />
                </div>
               <button type="submit" className="btn btn-primary" onClick={handleSubmitClick}>Register</button>
            </form>
            <div className="alert alert-success mt-2" style={{display: state.successMessage ? 'block' : 'none'}} role="alert">
                {state.successMessage}
            </div>
            <div className="mt-2">
                <span>Already have an account? </span>
                <span className="loginText" onClick={() => redirectToLogin()}>Login here</span>
            </div>
        </div>
    )
}

export default withRouter(RegistrationForm);