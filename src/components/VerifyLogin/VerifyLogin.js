import React from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import { ACCESS_TOKEN_NAME, API_BASE_URL } from '../../constants/apiConstants';
import axios from 'axios';
import queryString from 'query-string';

const VERIFY_LOGIN_SERVICE_URL = API_BASE_URL + '/magic-link';

class VerifyLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false,
            redirect: false,
            data: []
        };
    }

    componentDidMount() {
        this.verifyLogin();
    }

    verifyLoginWithAxios = () => {
        this.setState({...this.state, isFetching: true});

        let params = queryString.parse(this.props.location.search);
        console.log(JSON.stringify(params));
        const payload={
            "to": params.to,
            "token": params.token,
        }

        console.log("Payload: " + payload.token);
        console.log("Payload: " + payload.to);

        axios.put(VERIFY_LOGIN_SERVICE_URL, payload)
        .then(response => {
            this.setState({data: response.data, isFetching: false, redirect: true})
            localStorage.setItem(ACCESS_TOKEN_NAME, 'mytoken12345'); //response.data.token);
            this.props.history.push("/home?emailAddress=" + params.to);
        })
        .catch(e => {
            console.log(e);
            this.setState({...this.state, isFetching: false, success: false});
            this.props.history.push("/login");
        })
    };

    verifyLogin = this.verifyLoginWithAxios;

    render () {
        return (
            <div>Status: { this.state.data.status }{ this.state.data.status ? (<Redirect push to="/home"/>) : (<Redirect push to="/login"/>) }</div>
        )
    }
}

export default withRouter(VerifyLogin);